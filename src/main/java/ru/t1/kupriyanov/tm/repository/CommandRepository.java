package ru.t1.kupriyanov.tm.repository;

import ru.t1.kupriyanov.tm.api.ICommandRepository;
import ru.t1.kupriyanov.tm.constant.ArgumentConst;
import ru.t1.kupriyanov.tm.constant.CommandConst;
import ru.t1.kupriyanov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show info about programmer.");

    public static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show version.");

    public static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show list arguments.");

    public static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system information.");

    public static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");

    public static final Command COMMANDS = new Command(CommandConst.COMMANDS, ArgumentConst.COMMANDS, "Show command list.");

    public static final Command ARGUMENTS = new Command(CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show argument list.");

    public static final Command PROJECT_CREATE = new Command(CommandConst.PROJECT_CREATE, null, "Create a new project.");

    public static final Command PROJECT_LIST = new Command(CommandConst.PROJECT_LIST, null, "Show project list.");

    public static final Command PROJECT_CLEAR = new Command(CommandConst.PROJECT_CLEAR, null, "Remove all projects.");

    public static final Command TASK_CREATE = new Command(CommandConst.TASK_CREATE, null, "Create a new task.");

    public static final Command TASK_LIST = new Command(CommandConst.TASK_LIST, null, "Show task list.");

    public static final Command TASK_CLEAR = new Command(CommandConst.TASK_CLEAR, null, "Remove all tasks.");

    public static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, VERSION, HELP, INFO, COMMANDS,
            ARGUMENTS, EXIT, PROJECT_CREATE,
            PROJECT_LIST, PROJECT_CLEAR, TASK_CREATE,
            TASK_LIST, TASK_CLEAR};

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
