package ru.t1.kupriyanov.tm.service;

import ru.t1.kupriyanov.tm.api.ICommandRepository;
import ru.t1.kupriyanov.tm.api.ICommandService;
import ru.t1.kupriyanov.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
