package ru.t1.kupriyanov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
