package ru.t1.kupriyanov.tm.api;

import ru.t1.kupriyanov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
