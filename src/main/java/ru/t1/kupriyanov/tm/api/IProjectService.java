package ru.t1.kupriyanov.tm.api;

import ru.t1.kupriyanov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
